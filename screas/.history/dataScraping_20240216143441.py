from requests import get
from bs4 import BeautifulSoup
#     /vidpochynokvzakarpatti.html

BASE_URL = "https://zakarpattyachko.com.ua"
URL = f"{BASE_URL}/hoteli.html"

page = get(URL)
soup = BeautifulSoup(page.content, "html.parser")

fac_list = soup.find(class_="nav nav-list footer-list")
fac_list2 = soup.find(class_="dropdown-menu sub-menu")

for li in fac_list.find_all("li"):
    a = li.find("a")
    fac_name = a.find(text=True, recursive=False)
    fac_link = BASE_URL + a.get("href")
    print(f"Назва санаторії: {fac_name}")
    print(f"URL: {fac_link}")

for li in fac_list2.find_all("li"):
    a = li.find("a")
    rayon_name = a.find(text=True, recursive=False)
    rayon_link = BASE_URL + a.get("href")
    print(f"URL: {rayon_link}")