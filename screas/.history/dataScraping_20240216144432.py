import requests
from bs4 import BeautifulSoup

# URL джерела даних
BASE_URL = "https://www.ukma.edu.ua/"

# 2. Завантаження та виведення сторінки зі списком факультетів
faculty_url = f"{BASE_URL}/ua/General/struct"
response = requests.get(faculty_url)
print(response.text)

# 3. Отримання списку факультетів та їх URL за допомогою BeautifulSoup
soup = BeautifulSoup(response.text, "html.parser")
faculties = soup.find_all("a", class_="link-default")

# Створимо словник для зберігання факультетів та їх URL
faculty_data = {}

for faculty in faculties:
    faculty_name = faculty.text.strip()
    faculty_link = BASE_URL + faculty["href"]
    faculty_data[faculty_name] = faculty_link

# 4. Завантаження списків з кожної сторінки факультету та збереження результатів в текстовий файл
for faculty_name, faculty_link in faculty_data.items():
    faculty_response = requests.get(faculty_link)
    faculty_soup = BeautifulSoup(faculty_response.text, "html.parser")
    # Отримання списку об'єктів або іншої інформації та збереження до текстового файлу
    # Наприклад, faculty_objects = faculty_soup.find_all("div", class_="object")
    with open(f"{faculty_name}_objects.txt", "w", encoding="utf-8") as file:
        file.write(faculty_soup.prettify())
        # або можна зберегти дані у більш зручному форматі, наприклад, як JSON або CSV

print("Дані успішно зібрано та збережено до текстових файлів.")