import requests
from bs4 import BeautifulSoup

# URL джерела даних
BASE_URL = "https://osvita.ua/schools/"

# 2. Завантаження та виведення сторінки зі списком шкіл
response = requests.get(BASE_URL)
print(response.text)

# 3. Отримання списку шкіл та їх URL за допомогою BeautifulSoup
soup = BeautifulSoup(response.text, "html.parser")
schools = soup.find_all("a", class_="si-title")

# Створимо словник для зберігання шкіл та їх URL
school_data = {}

for school in schools:
    school_name = school.text.strip()
    school_link = school["href"]
    school_data[school_name] = school_link

# 4. Завантаження списків з кожної сторінки школи та збереження результатів в текстовий файл
for school_name, school_link in school_data.items():
    school_response = requests.get(school_link)
    school_soup = BeautifulSoup(school_response.text, "html.parser")
    # Отримання списку об'єктів або іншої інформації та збереження до текстового файлу
    # Наприклад, school_objects = school_soup.find_all("div", class_="object")
    with open(f"{school_name}_objects.txt", "w", encoding="utf-8") as file:
        file.write(school_soup.prettify())
        # або можна зберегти дані у більш зручному форматі, наприклад, як JSON або CSV

print("Дані успішно зібрано та збережено до текстових файлів.")