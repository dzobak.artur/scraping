from requests import get
from bs4 import BeautifulSoup
#     /vidpochynokvzakarpatti.html

BASE_URL = "https://zakarpattyachko.com.ua"
URL = f"{BASE_URL}/hoteli.html"
FILE_NAME = "uzhnu.txt"
with open(FILE_NAME, "w", encoding="utf-8") as file:

    page = get(URL)
soup = BeautifulSoup(page.content, "html.parser")

sanatory_list = soup.find(class_="nav nav-list footer-list")
sanatory_list2 = soup.find(class_="level1 ps-container ps-active-y")

for li in sanatory_list.find_all("li"):
    a = li.find("a")
    sanatory_name = a.find(text=True, recursive=False)
    sanatory_link = BASE_URL + a.get("href")
    print(f"Назва санаторії: {sanatory_name}")
    print(f"URL: {sanatory_link}")

for div in sanatory_list2.find_all("div"):
    a = div.find("a")
    rayon_name = a.find(text=True, recursive=False)
    rayon_link = BASE_URL + a.get("href")
    print(f"URL: {rayon_link}")