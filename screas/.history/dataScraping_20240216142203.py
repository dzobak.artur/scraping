from requests import get
from bs4 import BeautifulSoup
#/hoteli.html

BASE_URL = "https://zakarpattyachko.com.ua"
URL = f"{BASE_URL}/vidpochynokvzakarpatti.html"
HEADERS = {
    "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/110.0.0.0 Safari/537.36"
}

page = get(URL, headers=HEADERS)
soup = BeautifulSoup(page.content,  "html.parser")
# елемент що має клас departments_unfolded
fac_list = soup.find(class_="nav nav-list footer-list")
fac_list2 = soup.find(class_="col-md-12 my_top_content")


#для кожного дочірнього елемента li
for li in fac_list.find_all("li"):
    #дочірній елемент a
    a = li.find("a")
    #знаходимо текст безпосередньо в контенті елементу  a
    fac_name = a.find(text=True, recursive=False)
    #URL складається з базового, та відносного, який записано в атрибуті href
    fac_link = BASE_URL+ a.get("href")

    print(f"Назва факультету: {fac_name}")
    print(f"URL: {fac_link}")

for div in fac_list2.find_all("div"):
    a = div.find("a")

    rayon_name = a.find(text=True, recursive=False)

    rayon_link = BASE_URL+ a.get("href")

    print(f"Назва району: {rayon_name}")