from requests import get
from bs4 import BeautifulSoup
#/hoteli.html     /vidpochynokvzakarpatti.html

BASE_URL = "https://zakarpattyachko.com.ua"
URL = f"{BASE_URL}/vidpochynokvzakarpatti.html"
FILE_NAME = "aweq.txt"

with open(FILE_NAME, "w", encoding="utf-8") as file:
    page = get(URL)
    soup = BeautifulSoup(page.content, "html.parser")

    sanatory_list = soup.find(class_="nav nav-list footer-list")
    sanatory_list2 = soup.find(class_="level1 ps-container ps-active-y")

    for li in sanatory_list.find_all("li"):
        a = li.find("a")
        sanatory_name = a.find(text=True, recursive=False)
        sanatory_link = BASE_URL + a.get("href")
        file.write(f"Назва санаторії: {sanatory_name}\n")
        file.write(f"URL: {sanatory_link}\n")
        sanat_page = get(sanatory_link)
        soup = BeautifulSoup(sanat_page.content, "html.parser")
        dep_list = soup.find(id_="")

    

print("Дані успішно записано до файлу.")